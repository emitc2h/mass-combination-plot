from Histogram import Histogram
from Graph import Graph
import ROOT, style, palette

## Set the ATLAS style
root_functions = ROOT.gROOT.GetListOfGlobalFunctions()
if not root_functions.Contains('ATLASLabel'):
    ROOT.gROOT.LoadMacro("AtlasStyle.C")
    ROOT.gROOT.LoadMacro("AtlasLabels.C")
    ROOT.SetAtlasStyle()

do_chi2 = True

## --------------------------------------------------------- ##
scale = {
    '100' : (2.30, 1.0),
    '105' : (1.82, 1.0),
    '110' : (1.79, 1.0),
    '115' : (1.50, 1.0),
    '120' : (1.45, 1.0),
    '125' : (1.42, 1.0),
    '130' : (1.58, 1.0),
    '135' : (1.81, 1.0),
    '140' : (2.46, 1.0),
    '145' : (3.51, 1.0),
    '150' : (5.85, 1.0),
}


hh_scale = {
    '100' : (2.30/0.860839117762, 1.0/0.860839117762),
    '105' : (1.82/0.823086689903, 1.0/0.823086689903),
    '110' : (1.79/0.82558845507, 1.0/0.82558845507),
    '115' : (1.50/0.843469693506, 1.0/0.843469693506),
    '120' : (1.45/0.907871461188, 1.0/0.907871461188),
    '125' : (1.42, 1.0),
    '130' : (1.58/1.17485465888, 1.0/1.17485465888),
    '135' : (1.81/1.44641953982, 1.0/1.44641953982),
    '140' : (2.46/1.87797220965, 1.0/1.87797220965),
    '145' : (3.51/2.60652024985, 1.0/2.60652024985),
    '150' : (5.85/3.90719830765, 1.0/3.90719830765),
}
    


## --------------------------------------------------------- ##
def rebin(h):
    """
    Rebins histograms
    """

    h.Rebin(2)
    return h



## --------------------------------------------------------- ##
def strip_error(h):
    """
    Creates a new histogram without statistical error
    """

    nbins = h.GetNbinsX()
    new_name = '{0}_nostat'.format(h.GetName())
    
    new_h = ROOT.TH1F(new_name, new_name, nbins, h.GetBinLowEdge(1), h.GetBinLowEdge(nbins) + h.GetBinWidth(nbins))

    for i in range(1, nbins+1):
        new_h.SetBinContent(i, h.GetBinContent(i))
        new_h.SetBinError(i, 0)

    return new_h



## --------------------------------------------------------- ##
def interpolated_template(mass, lo, hi, step, templates):
    """
    Makes a linearly interpolated template from given templates
    """

    if not (lo <= mass <= hi): return None

    ## determine the templates to be interpolated between
    lo_index = int(float((mass - lo)/step))
    hi_index = lo_index+1

    ## determine the interpolation factor
    lo_mass = lo + lo_index*step
    hi_mass = lo + hi_index*step

    factor = float(mass - lo_mass)/step

    ## Make the output template
    h = ROOT.TH1F('interpolated_mass_%.2f' % mass,
                  'interpolated_mass_%.2f' % mass,
                  templates[0].GetNbinsX(), 
                  templates[0].GetBinLowEdge(1),
                  templates[0].GetBinLowEdge(templates[0].GetNbinsX()) + templates[0].GetBinWidth(templates[0].GetNbinsX()))

    h.Sumw2()

    h.Add(templates[lo_index], 1.0-factor)
    if factor > 0.0:
        h.Add(templates[hi_index], factor)

    return h

    
    
## --------------------------------------------------------- ##
def get_ztt(ll=None, lh=None, hh=None):
    """
    Gets the Ztautau background histograms across channels
    """

    h = ROOT.TH1F('ztt', 'ztt', 30, 50, 200)
    rebin(h)
    h.Sumw2()

    ## leplep
    if not ll is None:
        if 'vbf' in categories.lower().split('.'):
            ll_vbf_hist = ll.Get('mmc_sbrw_pfrw_ztautau_mvavbf_2012')
            h.Add(ll_vbf_hist)

        if 'boosted' in categories.lower().split('.'):
            ll_boosted_hist = ll.Get('mmc_sbrw_pfrw_ztautau_mvaboost_2012')
            h.Add(ll_boosted_hist)

            
    ## lephad
    if not lh is None:
        if 'vbf' in categories.lower().split('.'):
            lh_hist = lh.Get('Z#tau#tau.mass_mmc_tau_lep_e.mu_vbf')
            h.Add(rebin(lh_hist))

        if 'boosted' in categories.lower().split('.'):
            lh_hist = lh.Get('Z#tau#tau.mass_mmc_tau_lep_e.mu_boosted')
            h.Add(rebin(lh_hist))

        
    ## hadhad
    if not hh is None:
        if 'vbf' in categories.lower().split('.'):
            hh_vbf_hist = hh.Get('hh_category_vbf_Ztautau_125_150_mmc1_mass')
            h.Add(rebin(hh_vbf_hist))

        if 'boosted' in categories.lower().split('.'):
            hh_boosted_hist = hh.Get('hh_category_boosted_Ztautau_125_150_mmc1_mass')
            h.Add(rebin(hh_boosted_hist))

    return h



## --------------------------------------------------------- ##
def get_h(mass, ll=None, lh=None, hh=None, append='', mu_obs=False):
    """
    Gets the 125 GeV higgs signal histograms across channels
    """

    h = ROOT.TH1F('h%d%s' % (mass, append), 'h%d' % mass, 30, 50, 200)
    rebin(h)
    h.Sumw2()

    index = 1
    if mu_obs:
        index = 0
    
    ## leplep
    if not ll is None:
        if 'vbf' in categories.lower().split('.'):
            ll_vbf_hist = ll.Get('mmc_sbrw_pfrw_h%d_mvavbf_2012' % mass)
            ll_vbf_hist.Scale(scale[str(mass)][index])
            h.Add(ll_vbf_hist)

        if 'boosted' in categories.lower().split('.'):
            ll_boosted_hist = ll.Get('mmc_sbrw_pfrw_h%d_mvaboost_2012' % mass)
            ll_boosted_hist.Scale(scale[str(mass)][index])
            h.Add(ll_boosted_hist)
            
    
    ## lephad
    if not lh is None:
        if 'vbf' in categories.lower().split('.'):
            lh_vbf_hist = lh.Get('H%d.mass_mmc_tau_lep_e.mu_vbf' % mass)
            lh_vbf_hist.Scale(scale[str(mass)][index])
            h.Add(rebin(lh_vbf_hist))

        if 'boosted' in categories.lower().split('.'):
            lh_boosted_hist = lh.Get('H%d.mass_mmc_tau_lep_e.mu_boosted' % mass)
            lh_boosted_hist.Scale(scale[str(mass)][index])
            h.Add(rebin(lh_boosted_hist))

        
    ## hadhad
    if not hh is None:
        if 'vbf' in categories.lower().split('.'):
            hh_vbf_hist = hh.Get('hh_category_vbf_Signal_%d_mmc1_mass' % mass)
            hh_vbf_hist.Scale(hh_scale[str(mass)][index])
            h.Add(rebin(hh_vbf_hist))

        if 'boosted' in categories.lower().split('.'):
            hh_boosted_hist = hh.Get('hh_category_boosted_Signal_%d_mmc1_mass' % mass)
            hh_boosted_hist.Scale(hh_scale[str(mass)][index])
            h.Add(rebin(hh_boosted_hist))
            
    return strip_error(h)



## --------------------------------------------------------- ##
def get_fake(ll=None, lh=None, hh=None):
    """
    Gets the fake tau data-driven histograms across channels
    """

    h = ROOT.TH1F('fake', 'fake', 30, 50, 200)
    rebin(h)
    h.Sumw2()

    ## leplep
    if not ll is None:
        if 'vbf' in categories.lower().split('.'):
            ll_vbf_hist = ll.Get('mmc_sbrw_pfrw_fakes_mvavbf_2012')
            h.Add(ll_vbf_hist)

        if 'boosted' in categories.lower().split('.'):
            ll_boosted_hist = ll.Get('mmc_sbrw_pfrw_fakes_mvaboost_2012')
            h.Add(ll_boosted_hist)
    
    ## lephad
    if not lh is None:
        if 'vbf' in categories.lower().split('.'):
            lh_hist = lh.Get('Fake #tau.mass_mmc_tau_lep_e.mu_vbf')
            h.Add(rebin(lh_hist))
            
        if 'boosted' in categories.lower().split('.'):
            lh_hist = lh.Get('Fake #tau.mass_mmc_tau_lep_e.mu_boosted')
            h.Add(rebin(lh_hist))

    ## hadhad
    if not hh is None:
        if 'vbf' in categories.lower().split('.'):
            hh_vbf_hist = hh.Get('hh_category_vbf_QCD_125_150_mmc1_mass')
            h.Add(rebin(hh_vbf_hist))

        if 'boosted' in categories.lower().split('.'):
            hh_boosted_hist = hh.Get('hh_category_boosted_QCD_125_150_mmc1_mass')
            h.Add(rebin(hh_boosted_hist))

    return h


## --------------------------------------------------------- ##
def get_others(ll=None, lh=None, hh=None):
    """
    Gets the fake tau data-driven histograms across channels
    """

    h = ROOT.TH1F('w', 'w', 30, 50, 200)
    rebin(h)
    h.Sumw2()

    ## leplep
    if not ll is None:
        if 'vbf' in categories.lower().split('.'):
            ll_vbf_hist = ll.Get('mmc_sbrw_pfrw_zll_mvavbf_2012')
            h.Add(ll_vbf_hist)
            ll_vbf_hist = ll.Get('mmc_sbrw_pfrw_top_mvavbf_2012')
            h.Add(ll_vbf_hist)
            #ll_vbf_hist = ll.Get('mmc_sbrw_pfrw_dibos_mvavbf_2012')
            #h.Add(ll_vbf_hist)

        if 'boosted' in categories.lower().split('.'):
            ll_boosted_hist = ll.Get('mmc_sbrw_pfrw_zll_mvaboost_2012')
            h.Add(ll_boosted_hist)
            ll_boosted_hist = ll.Get('mmc_sbrw_pfrw_top_mvaboost_2012')
            h.Add(ll_boosted_hist)
            #ll_boosted_hist = ll.Get('mmc_sbrw_pfrw_dibos_mvaboost_2012')
            #h.Add(ll_boosted_hist)

    ## lephad
    if not lh is None:
        if 'vbf' in categories.lower().split('.'):
            lh_hist = lh.Get('Zll.mass_mmc_tau_lep_e.mu_vbf')
            h.Add(rebin(lh_hist))
            lh_hist = lh.Get('Top.mass_mmc_tau_lep_e.mu_vbf')
            h.Add(rebin(lh_hist))
            lh_hist = lh.Get('WW,WZ,ZZ.mass_mmc_tau_lep_e.mu_vbf')
            h.Add(rebin(lh_hist))

        if 'boosted' in categories.lower().split('.'):
            lh_hist = lh.Get('Zll.mass_mmc_tau_lep_e.mu_boosted')
            h.Add(rebin(lh_hist))
            lh_hist = lh.Get('Top.mass_mmc_tau_lep_e.mu_boosted')
            h.Add(rebin(lh_hist))
            lh_hist = lh.Get('WW,WZ,ZZ.mass_mmc_tau_lep_e.mu_boosted')
            h.Add(rebin(lh_hist))
            
    ## hadhad
    if not hh is None:
        if 'vbf' in categories.lower().split('.'):
            hh_vbf_hist = hh.Get('hh_category_vbf_Others_125_150_mmc1_mass')
            h.Add(rebin(hh_vbf_hist))
            

        if 'boosted' in categories.lower().split('.'):
            hh_boosted_hist = hh.Get('hh_category_boosted_Others_125_150_mmc1_mass')
            h.Add(rebin(hh_boosted_hist))

    return h


## --------------------------------------------------------- ##
def get_data(ll=None, lh=None, hh=None):
    """
    Gets the fake tau data-driven histograms across channels
    """

    h = ROOT.TH1F('data', 'data', 30, 50, 200)
    rebin(h)
    h.Sumw2()

    ## leplep
    if not ll is None:
        if 'vbf' in categories.lower().split('.'):
            ll_vbf_hist = ll.Get('mmc_sbrw_pfrw_data_mvavbf_2012')
            h.Add(ll_vbf_hist)

        if 'boosted' in categories.lower().split('.'):
            ll_boosted_hist = ll.Get('mmc_sbrw_pfrw_data_mvaboost_2012')
            h.Add(ll_boosted_hist)
    
    ## lephad
    if not lh is None:
        if 'vbf' in categories.lower().split('.'):
            lh_hist = lh.Get('Data.mass_mmc_tau_lep_e.mu_vbf')
            h.Add(rebin(lh_hist))

        if 'boosted' in categories.lower().split('.'):
            lh_hist = lh.Get('Data.mass_mmc_tau_lep_e.mu_boosted')
            h.Add(rebin(lh_hist))

    ## hadhad
    if not hh is None:
        if 'vbf' in categories.lower().split('.'):
            hh_vbf_hist = hh.Get('hh_category_vbf_Data_125_150_mmc1_mass')
            h.Add(rebin(hh_vbf_hist))

        if 'boosted' in categories.lower().split('.'):
            hh_boosted_hist = hh.Get('hh_category_boosted_Data_125_150_mmc1_mass')
            h.Add(rebin(hh_boosted_hist))

    return h




#######################################################################
## MAIN
#######################################################################

categories_list = ['VBF.Boosted']

channels_list   = [['#font[52]{ee}+#font[52]{e}#mu+#mu#mu'],
                   ['#font[52]{#mu}#tau_{had}+#font[52]{e}#tau_{had}'],
                   ['#tau_{had}#tau_{had}'],
                   ['#font[52]{ee}+#font[52]{e}#mu+#mu#mu', '#font[52]{#mu}#tau_{had}+#font[52]{e}#tau_{had}', '#tau_{had}#tau_{had}'],
                   ['#font[52]{#mu}#tau_{had}+#font[52]{e}#tau_{had}', '#tau_{had}#tau_{had}']
                  ]

fake_label = [
    'Fake lepton',
    'Fake #tau',
    'Multijet',
    'Fakes',
    'Fakes'
]

directory = 'post-fit'

make_pseudo_data = False
ratio            = False

for categories in categories_list:
    for k, channels in enumerate(channels_list):
        
        ## Get files
        ll_file=None
        lh_file=None
        hh_file=None

        output_file_suffix = []

        if '#font[52]{ee}+#font[52]{e}#mu+#mu#mu' in channels:
            ll_file = ROOT.TFile('%s/leplep.root' % directory)
            output_file_suffix.append('ll')
    
        if '#font[52]{#mu}#tau_{had}+#font[52]{e}#tau_{had}' in channels:
            lh_file = ROOT.TFile('%s/lephad.root' % directory)
            lh_file.Print()
            output_file_suffix.append('lh')
    
        if '#tau_{had}#tau_{had}' in channels:
            hh_file = ROOT.TFile('%s/hadhad.root' % directory)
            output_file_suffix.append('hh')

        ## Get the histograms
        data    = get_data(lh=lh_file, hh=hh_file, ll=ll_file)

        h110    = get_h(110, lh=lh_file, hh=hh_file, ll=ll_file)
        h125    = get_h(125, lh=lh_file, hh=hh_file, ll=ll_file)
        h150    = get_h(150, lh=lh_file, hh=hh_file, ll=ll_file)

        h110_mu =  get_h(110, lh=lh_file, hh=hh_file, ll=ll_file, append='mu_obs', mu_obs=True)
        h125_mu =  get_h(125, lh=lh_file, hh=hh_file, ll=ll_file, append='mu_obs', mu_obs=True)
        h150_mu =  get_h(150, lh=lh_file, hh=hh_file, ll=ll_file, append='mu_obs', mu_obs=True)
        
        ztt     = get_ztt(lh=lh_file, hh=hh_file, ll=ll_file)
        others  = get_others(lh=lh_file, hh=hh_file, ll=ll_file)
        fake    = get_fake(lh=lh_file, hh=hh_file, ll=ll_file)

        ## Make the graph showing the comparison of the background-subtracted data to various signal templates

        ## only lephad for now
        if do_chi2:
        
            ## Make the background model
            background = ROOT.TH1F('background', 'background', 30, 50, 200)
            background.Sumw2()
            background.Reset()
            rebin(background)
            
            background.Add(ztt)
            background.Add(others)
            background.Add(fake)
            
            bkgsub_data = ROOT.TH1F('bkgsub_data', 'bkgsub_data', 30, 50, 200)
            bkgsub_data.Sumw2()
            bkgsub_data.Reset()
            rebin(bkgsub_data)
            
            bkgsub_data.Add(data)
            bkgsub_data.Add(background, -1)

            ## Instantiate the graph
            chi2 = Graph('Chi2-%s-%s' % (categories, '.'.join(output_file_suffix)), 'm^{MMC}_{#tau#tau} [GeV]', '#chi^{2} prob. (bkg.-sub. data vs. signal)')
            #chi2.add(palette.bandgreen, 'band', '#pm2#sigma')
            #chi2.add(palette.bandyellow, 'band', '#pm1#sigma')
            #chi2.add(palette.black, 'dashline', 'many toys #chi^{2}')
            chi2.add(palette.grey, 'dashline', '#chi^{2} fit to interpolation')
            chi2.add(palette.black, 'dotline', '#chi^{2} fit to templates')
            chi2.fixLegendCoord(0.6, 0.9, 0.64, 0.76)

            chi2.addLabel('ATLAS', 0.20, 0.85)
            chi2.addLabel('#intL dt = 20.3fb^{-1}', 0.70, 0.85)
            chi2.addLabel('#sqrt{s} = 8 TeV', 0.72, 0.78)
            if len(channels) > 1:
                chi2.addLabel('Combined', 0.20, 0.80)
            else:
                chi2.addLabel(', '.join(channels), 0.20, 0.80)
            chi2.addLabel(categories.replace('.', '+'), 0.20, 0.73)

            step = 0.1
            minimum = 100
            maximum = 150
            n_steps = int((maximum-minimum)/step)

            templates = []
            for mass in [100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150]:
                higgs = get_h(mass, lh=lh_file, hh=hh_file, ll=ll_file, append='template', mu_obs=True)

                # print 'MASS TEMPLATE : %d' % mass
                # print '='*60

                # for i in range(1, higgs.GetNbinsX()+1):
                #     print higgs.GetBinContent(i), bkgsub_data.GetBinContent(i)
                
                templates.append(higgs)
                chi2test = bkgsub_data.Chi2Test(higgs, 'CHI2 WW')
                chi2.fill(1, mass, chi2test)

            chi2_points = []
                
            for mass in [minimum + step*i for i in range(n_steps+1)]:
                higgs = interpolated_template(mass, 100, 150, 5, templates)
                chi2test = bkgsub_data.Chi2Test(higgs, 'CHI2 WW')
                chi2.fill(0, mass, chi2test)
                chi2_points.append((mass, chi2test))

            ## Locate minimum mass
            minimum_chi2 = 10000
            minimum_mass = 1000
            
            for point in chi2_points:
                chi2_point = point[1]
                mass       = point[0]

                if chi2_point < minimum_chi2:
                    minimum_chi2 = chi2_point
                    minimum_mass = mass

            ## Locating +-1 and +-2 sigma bands

            delta_plus_1_sigma = 100
            mass_plus_1_sigma = 100
            
            delta_plus_2_sigma = 100
            mass_plus_2_sigma = 100
            
            delta_minus_1_sigma = 100
            mass_minus_1_sigma = 100
            
            delta_minus_2_sigma = 100
            mass_minus_2_sigma = 100
            
            for point in chi2_points:
                chi2_point = point[1]
                mass       = point[0]

                if mass > minimum_mass:
                    
                    delta_chi2min_plus_1_sigma = abs((minimum_chi2 + 1) - chi2_point)
                    if delta_chi2min_plus_1_sigma < delta_plus_1_sigma:
                        delta_plus_1_sigma = delta_chi2min_plus_1_sigma
                        mass_plus_1_sigma = mass

                    delta_chi2min_plus_2_sigma = abs((minimum_chi2 + 4) - chi2_point)
                    if delta_chi2min_plus_2_sigma < delta_plus_2_sigma:
                        delta_plus_2_sigma = delta_chi2min_plus_2_sigma
                        mass_plus_2_sigma = mass

                if mass < minimum_mass:
                    
                    delta_chi2min_minus_1_sigma = abs((minimum_chi2 + 1) - chi2_point)
                    if delta_chi2min_minus_1_sigma < delta_minus_1_sigma:
                        delta_minus_1_sigma = delta_chi2min_minus_1_sigma
                        mass_minus_1_sigma = mass

                    delta_chi2min_minus_2_sigma = abs((minimum_chi2 + 4) - chi2_point)
                    if delta_chi2min_minus_2_sigma < delta_minus_2_sigma:
                        delta_minus_2_sigma = delta_chi2min_minus_2_sigma
                        mass_minus_2_sigma = mass

            print 'Minimum mass located at %.3f +%.3f -%.3f ++%.3f --%.3f' % (minimum_mass,
                                                                              mass_plus_1_sigma,
                                                                              mass_minus_1_sigma,
                                                                              mass_plus_2_sigma,
                                                                              mass_minus_2_sigma)

            chi2.addLine(palette.red, minimum_mass, 0, minimum_mass, 1)
            chi2.addBox(palette.bandyellow, mass_minus_2_sigma, 0, mass_plus_2_sigma, 1)
            chi2.addBox(palette.bandgreen, mass_minus_1_sigma, 0, mass_plus_1_sigma, 1)

            chi2.addLabel('m_{H#rightarrow#tau#tau}=%.1f^{+%.1f}_{-%.1f} GeV' % (minimum_mass, abs(minimum_mass-mass_plus_1_sigma), abs(minimum_mass-mass_minus_1_sigma)), 0.64, 0.22)
            
            chi2.draw('L')
                

            
        ## Make the combination mass plot
        plot = Histogram('Combination_mass_plot-%s-%s' % (categories, '.'.join(output_file_suffix)), '#font[52]{m}^{MMC}_{#tau#tau} [GeV]', 30, 50, 200)
        plot.yaxis_title = 'ln(1+S/B) w. Events / 10 GeV'

        if not make_pseudo_data:
            plot.add_filled( data, 'Data', 1, 'points', stack=False)

        plot.add_filled( h125_mu, '#font[52]{H}(125)#rightarrow #tau#tau #font[42]{(#mu=1.4)}', 2, 'line',     stack=False, on_error=True, both_legend=True)
        plot.add_filled( h110_mu, '#font[52]{H}(110)#rightarrow #tau#tau #font[42]{(#mu=1.8)}', ROOT.kBlue,   'line2',     stack=False, on_error=True, ratio_only=True, ratio_legend=True)
        plot.add_filled( h150_mu, '#font[52]{H}(150)#rightarrow #tau#tau #font[42]{(#mu=5.9)}', ROOT.kGreen+2,    'line3',     stack=False, on_error=True, ratio_only=True, ratio_legend=True)

        plot.add_filled( ztt,     '#font[52]{Z}#rightarrow #tau#tau', 64,  'fill')
        plot.add_filled( others,  'Others',                          205, 'fill')
        plot.add_filled( fake,    fake_label[k],                       3,   'fill')

        plot.add_label('ATLAS', 0.19, 0.90)
        if len(channels) > 1:
            plot.add_label('H#rightarrow #tau#tau %s' % categories.replace('.', '+'), 0.19, 0.845)
        else:
            plot.add_label('%s %s' % (', '.join(channels), categories.replace('.', '+')), 0.19, 0.845)
        plot.add_label('#scale[0.75]{#int} L dt = %.1f fb^{-1}' % 20.3, 0.19, 0.78)
        plot.add_label('#sqrt{s} = 8 TeV', 0.19, 0.72)

        if make_pseudo_data:
            if ratio:
                plot.make_ratio(0, [0,1,2])
            else:
                plot.make_subtraction(0, [0,1,2])
            plot.make_fake_data()
        else:
            if ratio:
                plot.make_ratio(0, [1,2,3])
            else:
                plot.make_subtraction(0, [1,2,3])

        plot.draw()
            

        
          
          
