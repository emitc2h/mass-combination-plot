import ROOT
import array, types, math, os
import style, palette

##################################################
## Helper functions and classes

class Component:

    ## -------------------------------------------- ##
    def __init__(self, name):
        """
        Constructor
        """

        self.name       = name
        self.parent_name= None
        self.color      = None
        self.style      = None
        self.stack      = None
        self.index      = None
        self.label      = None
        self.binning    = None
        self.shapesys   = None
        self.overallsys = None

        self.yaxis_title = 'yields'

        self.nbins    = None
        self.nominal  = None
        self.sys_up   = None
        self.sys_down = None

        self.event_yield = 0.0
        self.product = 1.0

        self.density = False

        self.on_error = True
        self.ratio_only = False
        self.ratio_legend = False
        self.both_legend = False

        self.make_legend_space = True
        self.tmp = None

        self.positioning = style.normal_positioning

        
    ## -------------------------------------------- ##
    def apply_style(self, h):
        """
        Apply style to the nominal histogram
        """
        
        h.SetLineColor(self.color)
        h.SetMarkerColor(self.color)
        h.SetFillColor(self.color)
        self.style.apply(h)

        ## Axes
        xaxis = h.GetXaxis()
        xaxis.SetLabelSize(self.positioning.x_label_size)
        xaxis.SetLabelOffset(self.positioning.x_label_offset)
        xaxis.SetTitleSize(self.positioning.x_title_size)
        xaxis.SetTitleOffset(self.positioning.x_title_offset)
        xaxis.SetTitle(self.label)

        yaxis = h.GetYaxis()
        yaxis.SetLabelSize(self.positioning.y_label_size)
        yaxis.SetTitleSize(self.positioning.y_title_size)
        yaxis.SetTitleOffset(self.positioning.y_title_offset)
        yaxis.SetTitle(self.yaxis_title)
        if self.density:
            yaxis.SetTitle(self.yaxis_title + ' / bin width')
        yaxis.SetNdivisions(508)
        
        

    ## -------------------------------------------- ##
    def initialize(self):
        """
        Prepare the internal TH1Fs to be filled
        """

        ## Calculate number of bins
        self.hi    = self.binning[-1]
        self.lo    = self.binning[0]

        ## Calculate the maximum bin width
        max_bin_width = 0
        for b in range(1,len(self.binning)):
            bin_width = self.binning[b] - self.binning[b-1]
            if bin_width > max_bin_width:
                max_bin_width = bin_width

        ## Calculate extra bins to leave 30% of the space on the x-axis for the legend
        if self.make_legend_space:
            self.legend_max = self.hi + (self.hi-self.lo)*0.76
            self.plot_binning = self.binning + [self.hi+i*max_bin_width for i in range(1, int((self.legend_max-self.hi)/max_bin_width))]
        else:
            self.legend_max = self.hi
            self.plot_binning = self.binning

        self.nbins = len(self.plot_binning) - 1

            
        ## Instantiate the nominal histogram
        self.nominal = ROOT.TH1F('%s.%s' % (self.name, self.parent_name),
                                 self.name,
                                 self.nbins,
                                 array.array('d', self.plot_binning))
        self.nominal.Sumw2()

        ## Instantiate the shape systematics histograms
        self.sys_up   = {}
        self.sys_down = {}
        for sys in self.shapesys:
            ## Histograms for upward variation
            self.sys_up[sys] = ROOT.TH1F('%s.%s_%s_up' % (self.name, self.parent_name, sys),
                                         '%s.%s_%s_up' % (self.name, self.parent_name, sys),
                                         self.nbins,
                                         array.array('d', self.plot_binning))
            self.sys_up[sys].Sumw2()

            ## Histogram for downward variation
            self.sys_down[sys] = ROOT.TH1F('%s.%s_%s_down' % (self.name, self.parent_name, sys),
                                           '%s.%s_%s_down' % (self.name, self.parent_name, sys),
                                           self.nbins,
                                           array.array('d', self.plot_binning))
            self.sys_down[sys].Sumw2()


    ## -------------------------------------------- ##
    def initialize_filled(self):
        """
        Prepare already filled TH1Fs
        """

        ## Retrieve binning
        self.nbins = self.nominal.GetNbinsX()
        self.binning    = []
        for i in range(1, self.nbins+2):
            self.binning.append(self.nominal.GetBinLowEdge(i))

        self.plot_binning = self.binning

        self.event_yield = self.nominal.GetSumOfWeights()

        self.shapesys = []

        self.apply_style(self.nominal)


    ## -------------------------------------------- ##
    def fill(self, value, weight=1.0, sys='', sys_direction='UP'):
        """
        Give an event to the Component
        Specify sys for filling a systematics histogram instead of the nominal
        """

        if sys:
            if sys_direction == 'UP':
                try:
                    self.sys_up[sys].Fill(value, weight)
                except KeyError:
                    pass
            elif sys_direction == 'DOWN':
                try:
                    self.sys_down[sys].Fill(value, weight)
                except KeyError:
                    pass
        else:
            self.nominal.Fill(value, weight)
            self.event_yield += weight/self.product



    ## -------------------------------------------- ##
    def normalize_shape_errors(self):
        """
        Adjust all shape variations to have the same normalization as the
        nominal histogram
        """

        norm_nom = self.nominal.GetSumOfWeights()
        
        for sys in self.sys_up.keys():
            norm_up   = self.sys_up[sys].GetSumOfWeights()
            norm_down = self.sys_down[sys].GetSumOfWeights()

            if norm_up > 0:
                self.sys_up[sys].Scale(norm_nom/norm_up)

            if norm_down > 0:
                self.sys_down[sys].Scale(norm_nom/norm_down)
            
            

    ## -------------------------------------------- ##
    def add_systematic_errors(self):
        """
        Add the systematic uncertainty to the statistical uncertainty
        Should be called for completely filled histograms only
        Note that the error calculation is done in relative terms
        """
        
        ## Figure out the overall systematic error contribution
        overall_error = 0
        for sys in self.overallsys:
            low  = abs(1.0 - sys[0])
            high = abs(1.0 - sys[1])
            ## Symmetrize the error
            overall_error += (low**2 + high**2)/2
        

        ## Normalize shape variations
        self.normalize_shape_errors()
            
        ## Loop over bins:
        for i in range(1, self.nbins+1):
            bin_nominal    = self.nominal.GetBinContent(i)
            if bin_nominal == 0: continue
            
            bin_stat_error = (self.nominal.GetBinError(i)/bin_nominal)**2
            
            ## Get the shape systematic for this bin
            bin_shape_error = 0
            for sys in self.sys_up.keys():
                low  = abs(bin_nominal - self.sys_down[sys].GetBinContent(i))/bin_nominal
                high = abs(bin_nominal - self.sys_up[sys].GetBinContent(i))/bin_nominal
                ## Symmetrize the error
                bin_shape_error += (low**2 + high**2)/2

            ## Calculate total error
            bin_error = bin_nominal*math.sqrt(bin_stat_error + overall_error + bin_shape_error)

            ## Set the new error
            self.nominal.SetBinError(i, bin_error)


    ## -------------------------------------------- ##
    def clean_negative_weights(self):
        """
        Apply algorithms to remove negative weights
        """
        #cluster_negative_bins(self.nominal)
        #remove_negative_bins(self.nominal)


    ## -------------------------------------------- ##
    def set_density(self):
        """
        Especially useful for variable binnings,
        Divides the bin yield by the bin width
        """
        
        self.density = True

        for i in range(1, self.nbins+1):
            bin_nominal = self.nominal.GetBinContent(i)
            bin_error   = self.nominal.GetBinError(i)
            bin_width   = self.nominal.GetBinWidth(i)

            if bin_width == 0: continue

            self.nominal.SetBinContent(i, bin_nominal/bin_width)
            self.nominal.SetBinError(i, bin_error/bin_width)
            
        

        

##################################################
## Main class

class Histogram:

    ## -------------------------------------------- ##
    def __init__(self, name, label, binning, lo=0, hi=1):
        """
        Constructor
        """

        self.name  = name
        self.label = label

        self.xlog = False
        self.ylog = False

        self.do_ratio = False

        self.do_subtraction = False

        self.do_fake_data = False

        self.show_yields = False

        self.make_legend_space = True

        self.density = False

        self.yaxis_title = 'yields'

        ## Convert integer number of bins to bin lists, take as is if a list is passed
        if isinstance(binning, types.ListType):
            self.binning = binning
        else:
            self.binning = [(lo + i*(hi-lo)/binning) for i in range(binning+1)]

        ## Internal count of the number of histograms
        self.n = -1

        ## List of components
        self.components = []

        ## List of text labels
        self.text_labels = []

        ## Select a positioning object
        self.positioning = style.normal_positioning
        

    ## -------------------------------------------- ##
    def add(self, name, color, sty, product=1.0, stack=True, on_error=False, ratio_only=False, ratio_legend=False, both_legends=False, shapesys=[], overallsys=[]):
        """
        Add an histogram
        """

        ## Increment the internal count
        self.n += 1
        
        new_component = Component(name)
        new_component.parent_name= self.name
        new_component.index      = self.n
        if isinstance(color, basestring):
            new_component.color      = ROOT.TColor.GetColor(color)
        else:
            new_component.color      = color
        new_component.style      = style.style1D[sty]
        new_component.stack      = stack
        new_component.on_error   = on_error
        new_component.ratio_only = ratio_only
        new_component.ratio_legend = ratio_legend
        new_component.both_legend = both_legend
        new_component.label      = self.label
        new_component.binning    = self.binning
        new_component.shapesys   = shapesys
        new_component.overallsys = overallsys
        new_component.product    = product
        new_component.yaxis_title = self.yaxis_title
        new_component.make_legend_space = self.make_legend_space

        new_component.initialize()

        self.components.append(new_component)

        return self.n
    

    ## -------------------------------------------- ##
    def get_component(self, name):
        for component in self.components:
            if component.name.split('#')[0] == name:
                return component


    ## -------------------------------------------- ##
    def add_filled(self, th1f, name, color, sty, stack=True, on_error=False, ratio_only=False, ratio_legend=False, both_legend=False, sys_up={}, sys_down={}, overallsys=[]):
        """
        Add a histogram which has already been filled
        User is responsible to make binning match with other histograms
        shape systematics must be a dictionary of the shape variations
        """

        self.n += 1

        new_component = Component(name)
        new_component.parent_name= self.name
        new_component.index      = self.n
        new_component.nominal    = th1f
        if isinstance(color, basestring):
            new_component.color      = ROOT.TColor.GetColor(color)
        else:
            new_component.color      = color
        new_component.style      = style.style1D[sty]
        new_component.stack      = stack
        new_component.on_error   = on_error
        new_component.ratio_only = ratio_only
        new_component.ratio_legend = ratio_legend
        new_component.both_legend = both_legend
        new_component.label      = self.label
        new_component.sys_up     = sys_up
        new_component.sys_down   = sys_down
        new_component.overallsys = overallsys
        new_component.yaxis_title = self.yaxis_title

        new_component.initialize_filled()

        self.components.append(new_component)

        return self.n


    ## -------------------------------------------- ##
    def add_label(self, text, x=0.5, y=0.5):
        """
        Add a new label to the plot
        """

        self.text_labels.append((text, x, y))

        
    ## -------------------------------------------- ##
    def fill(self, index, value, weight=1.0, sys='', sys_direction='UP'):
        """
        Give an event to the Histogram
        Use index to specfiy which component to fill
        Specify sys for filling a systematics histogram instead of the nominal
        """

        self.components[index].fill(value, weight, sys, sys_direction)


    ## -------------------------------------------- ##
    def make_canvas(self):
        """
        Instantiate the Canvas
        """

        self.canvas = ROOT.TCanvas('%s_canvas' % self.name,
                                   '%s_canvas' % self.name,
                                   0, 0, 600, 600)
        
        self.canvas.SetLeftMargin(0.20)
        self.canvas.SetLogx(self.xlog)
        self.canvas.SetLogy(self.ylog)
        self.main_pad = self.canvas


    ## -------------------------------------------- ##
    def prepare_components(self):
        """
        Add systematic uncertainties and
        treat negative bins
        """
        
        for component in self.components:
            component.add_systematic_errors()
            component.clean_negative_weights()
            component.positioning = self.positioning
            if self.density:
                component.set_density()


    ## -------------------------------------------- ##
    def make_ratio(self, index, excess=[]):
        """
        Add the ratio plot, the stack is to be compared
        to component N. index
        """

        ## Communicate to the class that the ratio is activated
        self.do_ratio = True
        self.positioning = style.ratio_positioning
        self.numerator_index = index
        self.excess_indices = excess
        
        ## Override the default canvas
        self.canvas = ROOT.TCanvas('%s_canvas_ratio' % self.name,
                                   '%s_canvas_ratio' % self.name,
                                   0, 0, 600, 800)

        ## Divide the canvas
        self.canvas.Divide(1, 2, 0.0, 0.01, 0)

        ## Adjust the main pad
        self.main_pad = self.canvas.cd(1)
        self.main_pad.SetPad(0.0, 0.25, 1.0, 1.0)
        self.main_pad.GetFrame().SetBorderMode(0)
        self.main_pad.SetBorderSize(5)
        self.main_pad.SetTopMargin(0.05)
        self.main_pad.SetRightMargin(0.05)
        self.main_pad.SetLeftMargin(0.20)
        self.main_pad.SetBottomMargin(0.0)
        if self.xlog: 
            self.main_pad.SetLogx()
        if self.ylog: 
            self.main_pad.SetLogy()

        ## Adjust the ratio pad 
        self.ratio_pad = self.canvas.cd(2)
        self.ratio_pad.SetPad(0.0, 0.0, 1.0, 0.25)
        self.ratio_pad.GetFrame().SetBorderMode(0)
        self.ratio_pad.SetTopMargin(0.0)
        self.ratio_pad.SetRightMargin(0.05)
        self.ratio_pad.SetLeftMargin(0.20)
        self.ratio_pad.SetBottomMargin(0.4)
        if self.xlog:
            self.ratio_pad.SetLogx()



    ## -------------------------------------------- ##
    def make_subtraction(self, index, excess=[]):
        """
        Add the ratio plot, the stack is to be compared
        to component N. index
        """

        ## Communicate to the class that the ratio is activated
        self.do_subtraction = True
        self.positioning = style.ratio_positioning
        self.minuend_index = index
        self.excess_indices = excess
        
        ## Override the default canvas
        self.canvas = ROOT.TCanvas('%s_canvas_sub' % self.name,
                                   '%s_canvas_sub' % self.name,
                                   0, 0, 600, 600)

        ## Divide the canvas
        self.canvas.Divide(1, 2, 0.0, 0.01, 0)

        ## Adjust the main pad
        self.main_pad = self.canvas.cd(1)
        self.main_pad.SetPad(0.0, 0.38, 1.0, 1.0)
        self.main_pad.GetFrame().SetBorderMode(0)
        self.main_pad.SetBorderSize(3)
        self.main_pad.SetTopMargin(0.05)
        self.main_pad.SetRightMargin(0.05)
        self.main_pad.SetLeftMargin(0.15)
        self.main_pad.SetBottomMargin(0.03)
        if self.xlog: 
            self.main_pad.SetLogx()
        if self.ylog: 
            self.main_pad.SetLogy()

        ## Adjust the sub pad 
        self.sub_pad = self.canvas.cd(2)
        self.sub_pad.SetPad(0.0, 0.0, 1.0, 0.37)
        self.sub_pad.GetFrame().SetBorderMode(0)
        self.sub_pad.SetTopMargin(0.05)
        self.sub_pad.SetRightMargin(0.05)
        self.sub_pad.SetLeftMargin(0.15)
        self.sub_pad.SetBottomMargin(0.3)
        if self.xlog:
            self.sub_pad.SetLogx()
            



    ## -------------------------------------------- ##
    def make_fake_data(self):

        self.do_fake_data = True
        
        return
            


    ## -------------------------------------------- ##
    def make_legend(self):
        """
        Makes a legend
        """

        self.legend = ROOT.TLegend(0.60,
                                   self.positioning.legend_ymax - self.n*self.positioning.legend_spacing,
                                   self.positioning.legend_xmax,
                                   self.positioning.legend_ymax)
        self.legend.SetFillColor(0)
        self.legend.SetFillStyle(0)
        self.legend.SetBorderSize(0)
        self.legend.SetTextSize(18)
        self.legend.SetTextFont(43)

        for component in self.components:
            ## Exclude empty histograms from the legend
            if component.event_yield == 0: continue

            if component.ratio_legend and not component.both_legend: continue

            th1f = component.nominal
            if component.stack:
                th1f = component.tmp
            
            if self.show_yields:
                if component.style.name == 'fill':
                    self.legend.AddEntry(th1f,
                                         '%s (%.1f)' % (component.name, component.event_yield),
                                         'F')
                elif 'line' in component.style.name:
                    self.legend.AddEntry(th1f,
                                         '%s (%.1f)' % (component.name, component.event_yield),
                                         'L')
                else:
                    self.legend.AddEntry(th1f,
                                         '%s (%.1f)' % (component.name, component.event_yield),
                                         'LPE')
            else:
                if component.style.name == 'fill':
                    self.legend.AddEntry(th1f,
                                         component.name,
                                         'F')
                elif 'line' in component.style.name:
                    self.legend.AddEntry(th1f,
                                         component.name,
                                         'L')
                else:
                    self.legend.AddEntry(th1f,
                                         component.name,
                                         'LPE')

        self.legend.AddEntry(self.error, '', 'F')



        ## Ratio legend
        self.ratio_legend = ROOT.TLegend(0.175,
                                         0.89 - self.n*(1.1*self.positioning.legend_spacing),
                                         0.40,
                                         0.89)
        self.ratio_legend.SetFillColor(0)
        self.ratio_legend.SetFillStyle(0)
        self.ratio_legend.SetBorderSize(0)
        self.ratio_legend.SetTextSize(17)
        self.ratio_legend.SetTextFont(43)

        for component in self.components:
            ## Exclude empty histograms from the legend
            if component.event_yield == 0: continue

            if not component.ratio_legend and not component.both_legend: continue

            th1f = component.nominal
            if component.stack:
                th1f = component.tmp
            
            if self.show_yields:
                if component.style.name == 'fill':
                    self.ratio_legend.AddEntry(th1f,
                                               '%s (%.1f)' % (component.name, component.event_yield),
                                               'F')
                elif component.style.name == 'line':
                    self.ratio_legend.AddEntry(th1f,
                                               '%s (%.1f)' % (component.name, component.event_yield),
                                               'L')
                else:
                    self.ratio_legend.AddEntry(th1f,
                                               '%s (%.1f)' % (component.name, component.event_yield),
                                               'LP')
            else:
                if component.style.name == 'fill':
                    self.ratio_legend.AddEntry(th1f,
                                               component.name,
                                               'F')
                elif component.style.name == 'line':
                    self.ratio_legend.AddEntry(th1f,
                                               component.name,
                                               'L')
                else:
                    self.ratio_legend.AddEntry(th1f,
                                               component.name,
                                               'LP')
                
        return


    ## -------------------------------------------- ##
    def draw_ratio(self):
        """
        Draws the ratio, to be called within the main draw method
        """

        denominator = self.error

        if self.do_fake_data:
            numerator_component = self.components[0]
            numerator = self.fake_data
        else:
            numerator_component = self.components[self.numerator_index]
            numerator = numerator_component.nominal

        ## Prepare ratio histogram
        self.ratio =  ROOT.TH1F('%s_ratio' % self.name,
                                '%s_ratio' % self.name,
                                numerator_component.nbins,
                                array.array('d', numerator_component.plot_binning))

        self.excess = []
        for i in self.excess_indices:
            excess = self.components[i]
            excess_i = ROOT.TH1F('%s_%s_excess' % (self.name, excess.name),
                                 '%s_%s_excess' % (self.name, excess.name),
                                 excess.nbins,
                                 array.array('d', excess.plot_binning))

            self.excess.append(excess_i)
        
        ## Figure out the ratio content and errors
        for j in range(1, numerator_component.nbins+1):
            num_content = numerator.GetBinContent(j)
            num_error   = numerator.GetBinError(j)
            den_content = denominator.GetBinContent(j)
            den_error   = denominator.GetBinError(j)
            
            ## Skip bins with empty denominator
            if den_content == 0:
                self.ratio.SetBinContent(j, -1)
                self.ratio.SetBinError(j, 0.0)
                continue
            
            ratio_content = num_content/den_content
            ratio_error   = math.sqrt((num_error/den_content)**2 + ((num_content*den_error)/(den_content**2))**2)

            self.ratio.SetBinContent(j, ratio_content)
            self.ratio.SetBinError(j, ratio_error)

            for i, index in enumerate(self.excess_indices):
                excess = self.components[index]
                exs_content = excess.nominal.GetBinContent(j)
                exs_error   = excess.nominal.GetBinError(j)
                
                exs_num_content = exs_content + den_content
                exs_num_error   = math.sqrt(exs_error**2 + den_error**2)
                
                excess_content = exs_num_content/den_content
                excess_error   = math.sqrt((exs_num_error/den_content)**2 + ((exs_num_content*den_error)/(den_content**2))**2)
                
                self.excess[i].SetBinContent(j, excess_content)
                self.excess[i].SetBinError(j, excess_error)
                

            
        ## Apply style to ratio plot
        style.style1D['points'].apply(self.ratio)

        self.ratio_pad.cd()
        self.ratio.SetMinimum(0.59)
        self.ratio.SetMaximum(1.41)
        self.ratio.Draw()
        
        ## Adjust ratio axes and range
        xaxis = self.ratio.GetXaxis()
        yaxis = self.ratio.GetYaxis()
        
        xaxis.SetTitle(self.label)
        xaxis.SetTitleOffset(1.0)
        xaxis.SetTitleSize(0.15)
        xaxis.SetLabelSize(0.12)
        xaxis.SetLabelOffset(0.015)
        xaxis.SetTickLength(0.055)
        
        yaxis.SetTitle('Data / Model')
        yaxis.SetTitleOffset(0.5)
        yaxis.SetTitleSize(0.125)
        yaxis.SetLabelSize(0.12)
        yaxis.SetNdivisions(507)

        ## Show grid
        self.ratio_pad.SetGridy(2)

        ## Deal with the excess curve
        for i, index in enumerate(self.excess_indices):
            excess = self.components[index]
            excess.style.apply(self.excess[i])
            self.excess[i].SetLineColor(excess.color)
            self.excess[i].SetMarkerColor(excess.color)
            self.excess[i].SetFillColor(excess.color)
            self.excess[i].Draw('SAME %s' % excess.style.draw_options)

        
        ## Draw the ratio points
        self.ratio.Draw('SAME %s' % style.style1D['points'].draw_options)
        self.canvas.Update()
        self.canvas.cd()

        ## Cover the main pad 0
        box  = ROOT.TBox()
        box.SetFillColor(ROOT.kWhite)
        box.DrawBox(0.12, 0.32, 0.20, 0.37)

        ## Place a new 0
        latex = ROOT.TLatex()
        latex.SetNDC()
        latex.SetTextSize(0.045)
        latex.DrawLatex(0.17, 0.33, '0')



    ## -------------------------------------------- ##
    def draw_subtraction(self):
        """
        Draws the subtraction plot, to be called within the main draw method
        """

        subtrahend = self.error

        if self.do_fake_data:
            minuend_component = self.components[0]
            minuend = self.fake_data
        else:
            minuend_component = self.components[self.minuend_index]
            minuend = minuend_component.nominal

        ## Prepare subtracted histogram
        self.diff =  ROOT.TH1F('%s_sub' % self.name,
                               '%s_sub' % self.name,
                               minuend_component.nbins,
                               array.array('d', minuend_component.plot_binning))
        self.diff.Sumw2()

        self.sub_error =  ROOT.TH1F('%s_sub_error' % self.name,
                                    '%s_sub_error' % self.name,
                                    minuend_component.nbins,
                                    array.array('d', minuend_component.plot_binning))
        self.sub_error.Sumw2()

        self.excess = []
        for i in self.excess_indices:
            excess = self.components[i]
            excess_i = ROOT.TH1F('%s_%s_excess' % (self.name, excess.name),
                                 '%s_%s_excess' % (self.name, excess.name),
                                 excess.nbins,
                                 array.array('d', excess.plot_binning))

            self.excess.append(excess_i)
        
        ## Figure out the ratio content and errors

        plot_min = 0.0
        plot_max = 0.0
        
        for i in range(1, minuend_component.nbins+1):
            min_content = minuend.GetBinContent(i)
            min_error   = minuend.GetBinError(i)
            sub_content = subtrahend.GetBinContent(i)
            sub_error   = subtrahend.GetBinError(i)
            
            diff_content = min_content - sub_content
            diff_error   = min_error #math.sqrt((min_error)**2 + (sub_error)**2)

            self.diff.SetBinContent(i, diff_content)
            self.diff.SetBinError(i, diff_error)
            
            self.sub_error.SetBinContent(i, 0)
            self.sub_error.SetBinError(i, sub_error)

            ## Get plot_min and plot_max
            if diff_content + min_error > plot_max:
                plot_max = diff_content + min_error

            if sub_error > plot_max:
                plot_max = sub_error

            if diff_content - min_error < plot_min:
                plot_min = diff_content - min_error

            if (-sub_error) < plot_min:
                plot_min = (-sub_error)
            
        ## Apply style to subtraction plot
        style.style1D['points'].apply(self.diff)
        style.style1D['error'].apply(self.sub_error)

        self.sub_pad.cd()
        self.diff.SetMinimum(plot_min*1.2)
        self.diff.SetMaximum(16)
        self.diff.Draw()
        
        ## Adjust ratio axes and range
        xaxis = self.diff.GetXaxis()
        yaxis = self.diff.GetYaxis()
        
        xaxis.SetTitle(self.label)
        xaxis.SetTitleOffset(1.2)
        xaxis.SetTitleSize(0.12)
        xaxis.SetLabelSize(0.12)
        #xaxis.SetLabelOffset(0.02)
        xaxis.SetTickLength(0.055)
        
        yaxis.SetTitle('w. Data-Bkg.')
        yaxis.SetTitleOffset(0.50)
        yaxis.SetTitleSize(0.11)
        yaxis.SetLabelSize(0.125)
        yaxis.SetNdivisions(503)

        ## Show grid
        #self.sub_pad.SetGridy(1)

        ## Draw a reference line
        line = ROOT.TLine()
        line.DrawLine(self.diff.GetBinLowEdge(1),
                      0,
                      self.diff.GetBinLowEdge(self.diff.GetNbinsX())+self.diff.GetBinWidth(self.diff.GetNbinsX()),
                      0)
        
        ## Deal with the excess curve
        for i, index in enumerate(reversed(self.excess_indices)):
            excess = self.components[index]
            excess.apply_style(excess.nominal)
            excess.nominal.Draw('SAME %s' % excess.style.draw_options)

        
        ## Draw the points
        self.sub_error.SetFillColor(13)
        self.sub_error.SetLineColor(10)
        self.sub_error.SetMarkerColor(10)
        self.sub_error.Draw('SAME %s' % style.style1D['error'].draw_options)
        self.diff.Draw('SAME %s' % style.style1D['points'].draw_options)
        self.canvas.Update()
        self.canvas.cd()

        ## Print out the bins
        print '='*60
        print 'Difference for %s' % self.name
        for i in range(1, self.diff.GetNbinsX()):
            print 'bin %d : %.2f +- %.2f, (model error: %.2f)' % (i, self.diff.GetBinContent(i), self.diff.GetBinError(i), self.sub_error.GetBinError(i))
        print '-'*60

        # ## Cover the main pad 0
        # box  = ROOT.TBox()
        # box.SetFillColor(ROOT.kWhite)
        # box.DrawBox(0.10, 0.30, 0.147, 0.37)

        # ## Place a new 0
        # latex = ROOT.TLatex()
        # latex.SetNDC()
        # latex.SetTextSize(0.05)
        # latex.DrawLatex(0.122, 0.33, '0')

        

    ## -------------------------------------------- ##
    def draw(self):
        """
        Draw the entire thing,
        print to file
        """

        ## Prepare components
        self.prepare_components()

        ## Canvas
        
        if self.do_ratio or self.do_subtraction:
            self.main_pad.cd()
        else:
            self.make_canvas()
            self.canvas.cd()
            
        ## Prepare the error histogram
        for component in self.components:
            if component.ratio_only: continue
            if component.stack:
                error_name = '#font[42]{Uncert.}'
                if len(component.shapesys) > 0 or len(component.overallsys) > 0:
                    error_name = '#font[42]{Uncert.}'
                self.error = ROOT.TH1F('%s_error' % self.name,
                                       error_name,
                                       component.nbins,
                                       array.array('d', component.plot_binning))
                break

        ## First, draw the stack
        first = True
        first_component = None
        for i, component_i in enumerate(self.components):
            if component_i.ratio_only: continue
            component_i.tmp = ROOT.TH1F('%s_%s_tmp' % (component_i.name, self.name),
                                        'tmp',
                                        component_i.nbins,
                                        array.array('d', component_i.plot_binning))

            component_i.tmp.Sumw2()
            component_i.tmp.Add(component_i.nominal)
            
            if component_i.stack:
                self.error.Add(component_i.nominal)
                
                for j, component_j in enumerate(self.components):
                    if component_j.ratio_only: continue
                    if j>i and component_j.stack:
                        component_i.tmp.Add(component_j.nominal)

                component_i.apply_style(component_i.tmp)
                if first:
                    component_i.tmp.Draw('%s' % component_i.style.draw_options)
                    first_component = component_i
                    first = False
                else:
                    component_i.tmp.Draw('SAME %s' % component_i.style.draw_options)

                    
        ## Then draw the error
        style.style1D['error'].apply(self.error)
        self.error.SetFillColor(13)
        self.error.SetLineColor(10)
        self.error.SetMarkerColor(10)
        self.error.Draw('SAME %s' % style.style1D['error'].draw_options)

        
        ## Find the plot maximum
        bin_max = self.error.GetMaximumBin()
        maximum = self.error.GetBinContent(bin_max) + self.error.GetBinError(bin_max)

        
        ## Then draw non-stacked histograms
        for component in reversed(self.components):
            if component.stack: continue
            if component.ratio_only: continue
            
            if component.on_error:
                component.tmp.Add(self.error)
            
            bin_max = component.tmp.GetMaximumBin()
            this_maximum = component.tmp.GetBinContent(bin_max) + component.tmp.GetBinError(bin_max)
            if this_maximum > maximum:
                maximum = this_maximum

            component.apply_style(component.tmp)
                
            if first:
                component.tmp.Draw('%s' % component.style.draw_options)
                first_component = component
                first = False
            else:
                component.tmp.Draw('SAME %s' % component.style.draw_options)


        ## Draw fake data
        if self.do_fake_data:
            self.fake_data =  ROOT.TH1F('%s_fake_data' % self.name,
                                        '%s_fake_data' % self.name,
                                        self.components[0].nbins,
                                        array.array('d', self.components[0].plot_binning))
        
            self.fake_data.Add(self.error)
            if len(self.excess_indices) > 0:
                self.fake_data.Add(self.components[self.excess_indices[0]].nominal)

            style.style1D['points'].apply(self.fake_data)
            self.fake_data.Draw('SAME %s' % style.style1D['points'].draw_options)

                
        ## Set the plot y range
        if self.ylog:
            if first_component.stack:
                first_component.tmp.SetMaximum(1000*maximum)
                first_component.tmp.SetMinimum(0.9)
            else:
                first_component.nominal.SetMaximum(1000*maximum)
                first_component.nominal.SetMinimum(0.9)
        else:
            if first_component.stack:
                first_component.tmp.SetMaximum(1.5*maximum)
                first_component.tmp.SetMinimum(0.0)
            else:
                first_component.nominal.SetMaximum(1.5*maximum)
                first_component.nominal.SetMinimum(0.0)
        self.canvas.Update()

        
        ## Take care of the ratio business
        if self.do_ratio:
            self.draw_ratio()

            
        ## Take care of the subtraction business
        if self.do_subtraction:
            self.draw_subtraction()

        ## Make the legend
        self.make_legend()
        self.legend.Draw('SAME')

        if self.do_subtraction or self.do_ratio:
            self.sub_pad.cd()
            self.ratio_legend.Draw('SAME')
            self.canvas.cd()
        
        ## Place labels
        latex = ROOT.TLatex()
        latex.SetNDC()
        latex.SetTextColor(1)
        latex.SetTextSize(22)
        latex.SetTextFont(43)

        for lbl in self.text_labels:

            x, y = self.positioning.plot_label_offset(lbl[1], lbl[2])
            
            if lbl[0] == 'ATLAS':
                latex.SetTextFont(73)
                latex.DrawLatex(x, y, 'ATLAS')
                latex.SetTextFont(43)
                latex.DrawLatex(x+0.13, y, 'Preliminary')
            else:
                latex.DrawLatex(x, y, lbl[0])


        ## Redraw the axis
        self.main_pad.RedrawAxis()
                
        
        ## Print to file
        self.canvas.Print('%s.png' % self.name)
        self.canvas.Print('%s.eps' % self.name)
        self.canvas.Print('%s.pdf' % self.name)
            

    ## -------------------------------------------- ##
    def save(self):
        """
        Save the Histogram to a ROOT File
        """

        cwd = os.getcwd()
        
        ## Make destination directory
        path = './root'
        try:
            os.makedirs(path)
        except OSError:
            pass

        ## Move to the new directory
        os.chdir(path)

        ## Write everything to a ROOT file
        f = ROOT.TFile('%s.root' % self.name, 'RECREATE')
        f.cd()
        for component in self.components:
            component.nominal.Write()
        self.error.Write()
        self.legend.Write()
        if self.do_ratio:
            self.ratio.Write()
        self.canvas.Write()
        f.Close()

        ## Move back to original driectory
        os.chdir(cwd)
        
