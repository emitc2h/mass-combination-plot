import ROOT, math

##############################################
def convert_lephad(input_histogram):
    
    output = ROOT.TH1F(input_histogram.GetName(), input_histogram.GetName(), 30, 50, 200)
    output.Sumw2()
    
    lo = 10
    hi = 41
    j = 1
    
    for i in range(1, input_histogram.GetNbinsX()+1):
        if lo < i < hi:
            bin_content = input_histogram.GetBinContent(i)
            bin_error   = input_histogram.GetBinError(i)
            output.SetBinContent(j, bin_content)
            output.SetBinError(j, bin_error)
            j+=1

    return output


##############################################
def convert_leplep(input_histogram):
    
    output = ROOT.TH1F(input_histogram.GetName(), input_histogram.GetName(), 15, 50, 200)
    output.Sumw2()
    
    lo = 5
    hi = 21
    j = 1
    
    for i in range(1, input_histogram.GetNbinsX()+1):
        if lo < i < hi:
            bin_content = input_histogram.GetBinContent(i)
            bin_error   = input_histogram.GetBinError(i)
            output.SetBinContent(j, bin_content)
            output.SetBinError(j, bin_error)
            j+=1

    return output


##############################################
def convert_hadhad(input_histogram):
    return input_histogram


##############################################
class Histo:
    """
    A class to retrieve and record the histograms to stack up together in the combination plots
    """
    
    ## ---------------------------------------------------- ##
    def __init__(self):
        """
        Constructor
        """
        self.input_file  = None
        self.input_names = None
        self.error_file  = None
        self.error_names = None
        self.output_name = None
        self.histogram   = None

    ## ---------------------------------------------------- ##
    def add_error(self, convert):
        """
        Add error in quadrature from an external histogram
        """
        f = ROOT.TFile(self.error_file)
        h = f.Get(self.error_names[0])
        for i in range(1, len(self.error_names)):
            h.Add( f.Get(self.error_names[i]) )
        error = convert(h)

        for i in range(1, error.GetNbinsX()+1):
            bin_new_error     = error.GetBinError(i)
            bin_current_error = self.histogram.GetBinError(i)
            
            self.histogram.SetBinError(i, math.sqrt(bin_new_error**2 + bin_current_error**2))

    ## ---------------------------------------------------- ##
    def get(self, convert, output_file):
        """
        Get the histogram
        """
        f = ROOT.TFile(self.input_file)
        h = f.Get(self.input_names[0])
        for i in range(1, len(self.input_names)):
            h.Add( f.Get(self.input_names[i]) )
        self.histogram = convert(h)
        self.histogram.SetDirectory(0)
        f.Close()

        if (not self.error_file is None) and (not self.error_names is None):
            self.add_error(convert)

        output_file.cd()

        self.histogram.Write(self.output_name)
        


#################################################################
## LEPHAD dictionary for plots                                 ##
#################################################################

lephad = []

## Data
data_vbf = Histo()
data_vbf.input_file  = 'lephad_workspace/Syst.em_FF_SR_2jetvbf_embsyks95sths1crand_mmc_mass_lnsobw.root'
data_vbf.input_names = ['data.h_SR_mmc_mass_lnsobw']
data_vbf.output_name = 'Data.mass_mmc_tau_lep_e.mu_vbf'
lephad.append(data_vbf)

data_boo = Histo()
data_boo.input_file  = 'lephad_workspace/Syst.em_FF_SR_1jetb_embsyks95sths1crand_mmc_mass_lnsobw.root'
data_boo.input_names = ['data.h_SR_mmc_mass_lnsobw']
data_boo.output_name = 'Data.mass_mmc_tau_lep_e.mu_boosted'
lephad.append(data_boo)


## Fake
fake_vbf = Histo()
fake_vbf.input_file  = 'lephad-output/lephad-output-125Httlh_em_FF_SR_2jetvbf_embsyks95sths1crand_mmc_mass_lnsobw.root'
fake_vbf.input_names = ['My_FitError_AfterFit_Fake_']
fake_vbf.error_file  = 'lephad_workspace/Syst.em_FF_SR_2jetvbf_embsyks95sths1crand_mmc_mass_lnsobw.root'
fake_vbf.error_names = ['FF.h_SR_mmc_mass_lnsobw_DEFAULT']
fake_vbf.output_name = 'Fake #tau.mass_mmc_tau_lep_e.mu_vbf'
lephad.append(fake_vbf)

fake_boo = Histo()
fake_boo.input_file  = 'lephad-output/lephad-output-125Httlh_em_FF_SR_1jetb_embsyks95sths1crand_mmc_mass_lnsobw.root'
fake_boo.input_names = ['My_FitError_AfterFit_Fake_']
fake_boo.error_file  = 'lephad_workspace/Syst.em_FF_SR_1jetb_embsyks95sths1crand_mmc_mass_lnsobw.root'
fake_boo.error_names = ['FF.h_SR_mmc_mass_lnsobw_DEFAULT']
fake_boo.output_name = 'Fake #tau.mass_mmc_tau_lep_e.mu_boosted'
lephad.append(fake_boo)


## Ztt
ztt_vbf = Histo()
ztt_vbf.input_file  = 'lephad-output/lephad-output-125Httlh_em_FF_SR_2jetvbf_embsyks95sths1crand_mmc_mass_lnsobw.root'
ztt_vbf.input_names = ['My_FitError_AfterFit_Ztt_']
ztt_vbf.error_file  = 'lephad_workspace/Syst.em_FF_SR_2jetvbf_embsyks95sths1crand_mmc_mass_lnsobw.root'
ztt_vbf.error_names = ['Embedding.h_SR_mmc_mass_lnsobw_DEFAULT']
ztt_vbf.output_name = 'Z#tau#tau.mass_mmc_tau_lep_e.mu_vbf'
lephad.append(ztt_vbf)

ztt_boo = Histo()
ztt_boo.input_file  = 'lephad-output/lephad-output-125Httlh_em_FF_SR_1jetb_embsyks95sths1crand_mmc_mass_lnsobw.root'
ztt_boo.input_names = ['My_FitError_AfterFit_Ztt_']
ztt_boo.error_file  = 'lephad_workspace/Syst.em_FF_SR_1jetb_embsyks95sths1crand_mmc_mass_lnsobw.root'
ztt_boo.error_names = ['Embedding.h_SR_mmc_mass_lnsobw_DEFAULT']
ztt_boo.output_name = 'Z#tau#tau.mass_mmc_tau_lep_e.mu_boosted'
lephad.append(ztt_boo)


## Top
top_vbf = Histo()
top_vbf.input_file  = 'lephad-output/lephad-output-125Httlh_em_FF_SR_2jetvbf_embsyks95sths1crand_mmc_mass_lnsobw.root'
top_vbf.input_names = ['My_FitError_AfterFit_Top_']
top_vbf.error_file  = 'lephad_workspace/Syst.em_FF_SR_2jetvbf_embsyks95sths1crand_mmc_mass_lnsobw.root'
top_vbf.error_names = ['ttbar.h_SR_mmc_mass_lnsobw_DEFAULT']
top_vbf.output_name = 'Top.mass_mmc_tau_lep_e.mu_vbf'
lephad.append(top_vbf)

top_boo = Histo()
top_boo.input_file  = 'lephad-output/lephad-output-125Httlh_em_FF_SR_1jetb_embsyks95sths1crand_mmc_mass_lnsobw.root'
top_boo.input_names = ['My_FitError_AfterFit_Top_']
top_boo.error_file  = 'lephad_workspace/Syst.em_FF_SR_1jetb_embsyks95sths1crand_mmc_mass_lnsobw.root'
top_boo.error_names = ['ttbar.h_SR_mmc_mass_lnsobw_DEFAULT']
top_boo.output_name = 'Top.mass_mmc_tau_lep_e.mu_boosted'
lephad.append(top_boo)


## Zll
zll_vbf = Histo()
zll_vbf.input_file  = 'lephad-output/lephad-output-125Httlh_em_FF_SR_2jetvbf_embsyks95sths1crand_mmc_mass_lnsobw.root'
zll_vbf.input_names = ['My_FitError_AfterFit_Zl2t_', 'My_FitError_AfterFit_Zj2t_']
zll_vbf.error_file  = 'lephad_workspace/Syst.em_FF_SR_2jetvbf_embsyks95sths1crand_mmc_mass_lnsobw.root'
zll_vbf.error_names = ['Zltau.h_SR_mmc_mass_lnsobw_DEFAULT', 'Zlljet.h_SR_mmc_mass_lnsobw_DEFAULT']
zll_vbf.output_name = 'Zll.mass_mmc_tau_lep_e.mu_vbf'
lephad.append(zll_vbf)

zll_boo = Histo()
zll_boo.input_file  = 'lephad-output/lephad-output-125Httlh_em_FF_SR_1jetb_embsyks95sths1crand_mmc_mass_lnsobw.root'
zll_boo.input_names = ['My_FitError_AfterFit_Zl2t_', 'My_FitError_AfterFit_Zj2t_']
zll_boo.error_file  = 'lephad_workspace/Syst.em_FF_SR_1jetb_embsyks95sths1crand_mmc_mass_lnsobw.root'
zll_boo.error_names = ['Zltau.h_SR_mmc_mass_lnsobw_DEFAULT', 'Zlljet.h_SR_mmc_mass_lnsobw_DEFAULT']
zll_boo.output_name = 'Zll.mass_mmc_tau_lep_e.mu_boosted'
lephad.append(zll_boo)


## Diboson
vv_vbf = Histo()
vv_vbf.input_file  = 'lephad-output/lephad-output-125Httlh_em_FF_SR_2jetvbf_embsyks95sths1crand_mmc_mass_lnsobw.root'
vv_vbf.input_names = ['My_FitError_AfterFit_VV_']
vv_vbf.error_file  = 'lephad_workspace/Syst.em_FF_SR_2jetvbf_embsyks95sths1crand_mmc_mass_lnsobw.root'
vv_vbf.error_names = ['diboson.h_SR_mmc_mass_lnsobw_DEFAULT']
vv_vbf.output_name = 'WW,WZ,ZZ.mass_mmc_tau_lep_e.mu_vbf'
lephad.append(vv_vbf)

vv_boo = Histo()
vv_boo.input_file  = 'lephad-output/lephad-output-125Httlh_em_FF_SR_1jetb_embsyks95sths1crand_mmc_mass_lnsobw.root'
vv_boo.input_names = ['My_FitError_AfterFit_VV_']
vv_boo.error_file  = 'lephad_workspace/Syst.em_FF_SR_1jetb_embsyks95sths1crand_mmc_mass_lnsobw.root'
vv_boo.error_names = ['diboson.h_SR_mmc_mass_lnsobw_DEFAULT']
vv_boo.output_name = 'WW,WZ,ZZ.mass_mmc_tau_lep_e.mu_boosted'
lephad.append(vv_boo)


for mass in [100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150]:
    ## Higgs mass points
    higgs_vbf = Histo()
    higgs_vbf.input_file  = 'lephad-output/lephad-output-%dHttlh_em_FF_SR_2jetvbf_embsyks95sths1crand_mmc_mass_lnsobw.root' % mass
    higgs_vbf.input_names = ['My_BeforeFitMu1_ggH_', 'My_BeforeFitMu1_VBF_', 'My_BeforeFitMu1_WH_', 'My_BeforeFitMu1_ZH_']
    higgs_vbf.output_name = 'H%d.mass_mmc_tau_lep_e.mu_vbf' % mass
    lephad.append(higgs_vbf)

    higgs_boo = Histo()
    higgs_boo.input_file  = 'lephad-output/lephad-output-%dHttlh_em_FF_SR_1jetb_embsyks95sths1crand_mmc_mass_lnsobw.root' % mass
    higgs_boo.input_names = ['My_BeforeFitMu1_ggH_', 'My_BeforeFitMu1_VBF_', 'My_BeforeFitMu1_WH_', 'My_BeforeFitMu1_ZH_']
    higgs_boo.output_name = 'H%d.mass_mmc_tau_lep_e.mu_boosted' % mass
    lephad.append(higgs_boo)



#################################################################
## LEPLEP dictionary for plots                                 ##
#################################################################

leplep = []

## Data
data_vbf = Histo()
data_vbf.input_file  = 'leplep_workspace/Shapes_11_10_2013_sbmmc_fixprun.root'
data_vbf.input_names = ['data_mvavbfcat_2012']
data_vbf.output_name = 'mmc_sbrw_pfrw_data_mvavbf_2012'
leplep.append(data_vbf)

data_boo = Histo()
data_boo.input_file  = 'leplep_workspace/Shapes_11_10_2013_sbmmc_fixprun.root'
data_boo.input_names = ['data_mvaboostcat_2012']
data_boo.output_name = 'mmc_sbrw_pfrw_data_mvaboost_2012'
leplep.append(data_boo)


## Fake
fake_vbf = Histo()
fake_vbf.input_file  = 'leplep-output/leplep-output-125ll12_vbf.root'
fake_vbf.input_names = ['My_FitError_AfterFit_Fake_']
fake_vbf.error_file  = 'leplep_workspace/Shapes_11_10_2013_sbmmc_fixprun.root'
fake_vbf.error_names = ['bkgfakes_nominal_mvavbfcat_2012']
fake_vbf.output_name = 'mmc_sbrw_pfrw_fakes_mvavbf_2012'
leplep.append(fake_vbf)

fake_boo = Histo()
fake_boo.input_file  = 'leplep-output/leplep-output-125ll12_boost.root'
fake_boo.input_names = ['My_FitError_AfterFit_Fake_']
fake_boo.error_file  = 'leplep_workspace/Shapes_11_10_2013_sbmmc_fixprun.root'
fake_boo.error_names = ['bkgfakes_nominal_mvaboostcat_2012']
fake_boo.output_name = 'mmc_sbrw_pfrw_fakes_mvaboost_2012'
leplep.append(fake_boo)


## Ztt
ztt_vbf = Histo()
ztt_vbf.input_file  = 'leplep-output/leplep-output-125ll12_vbf.root'
ztt_vbf.input_names = ['My_FitError_AfterFit_Zttee_', 'My_FitError_AfterFit_Zttmm_', 'My_FitError_AfterFit_Zttem_', 'My_FitError_AfterFit_Zttme_']
ztt_vbf.error_file  = 'leplep_workspace/Shapes_11_10_2013_sbmmc_fixprun.root'
ztt_vbf.error_names = ['bkgembee_nominal_mvavbfcat_2012', 'bkgembem_nominal_mvavbfcat_2012', 'bkgembme_nominal_mvavbfcat_2012', 'bkgembmm_nominal_mvavbfcat_2012']
ztt_vbf.output_name = 'mmc_sbrw_pfrw_ztautau_mvavbf_2012'
leplep.append(ztt_vbf)

ztt_boo = Histo()
ztt_boo.input_file  = 'leplep-output/leplep-output-125ll12_boost.root'
ztt_boo.input_names = ['My_FitError_AfterFit_Zttee_', 'My_FitError_AfterFit_Zttmm_', 'My_FitError_AfterFit_Zttem_', 'My_FitError_AfterFit_Zttme_']
ztt_boo.error_file  = 'leplep_workspace/Shapes_11_10_2013_sbmmc_fixprun.root'
ztt_boo.error_names = ['bkgembee_nominal_mvaboostcat_2012', 'bkgembem_nominal_mvaboostcat_2012', 'bkgembme_nominal_mvaboostcat_2012', 'bkgembmm_nominal_mvaboostcat_2012']
ztt_boo.output_name = 'mmc_sbrw_pfrw_ztautau_mvaboost_2012'
leplep.append(ztt_boo)


## Top
top_vbf = Histo()
top_vbf.input_file  = 'leplep-output/leplep-output-125ll12_vbf.root'
top_vbf.input_names = ['My_FitError_AfterFit_Top_']
top_vbf.error_file  = 'leplep_workspace/Shapes_11_10_2013_sbmmc_fixprun.root'
top_vbf.error_names = ['bkgtop_nominal_mvavbfcat_2012']
top_vbf.output_name = 'mmc_sbrw_pfrw_top_mvavbf_2012'
leplep.append(top_vbf)

top_boo = Histo()
top_boo.input_file  = 'leplep-output/leplep-output-125ll12_boost.root'
top_boo.input_names = ['My_FitError_AfterFit_Top_']
top_boo.error_file  = 'leplep_workspace/Shapes_11_10_2013_sbmmc_fixprun.root'
top_boo.error_names = ['bkgtop_nominal_mvaboostcat_2012']
top_boo.output_name = 'mmc_sbrw_pfrw_top_mvaboost_2012'
leplep.append(top_boo)


## Zll-dibson
zll_vbf = Histo()
zll_vbf.input_file  = 'leplep-output/leplep-output-125ll12_vbf.root'
zll_vbf.input_names = ['My_FitError_AfterFit_Other_']
zll_vbf.error_file  = 'leplep_workspace/Shapes_11_10_2013_sbmmc_fixprun.root'
zll_vbf.error_names = ['bkgother_nominal_mvavbfcat_2012']
zll_vbf.output_name = 'mmc_sbrw_pfrw_zll_mvavbf_2012'
leplep.append(zll_vbf)

zll_boo = Histo()
zll_boo.input_file  = 'leplep-output/leplep-output-125ll12_boost.root'
zll_boo.input_names = ['My_FitError_AfterFit_Other_']
zll_boo.error_file  = 'leplep_workspace/Shapes_11_10_2013_sbmmc_fixprun.root'
zll_boo.error_names = ['bkgother_nominal_mvaboostcat_2012']
zll_boo.output_name = 'mmc_sbrw_pfrw_zll_mvaboost_2012'
leplep.append(zll_boo)


for mass in [100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150]:
    ## Higgs mass points
    higgs_vbf = Histo()
    higgs_vbf.input_file  = 'leplep-output/leplep-output-%dll12_vbf.root' % mass
    higgs_vbf.input_names = ['My_BeforeFitMu1_ggH_', 'My_BeforeFitMu1_VBF_', 'My_BeforeFitMu1_WH_', 'My_BeforeFitMu1_ZH_']
    higgs_vbf.output_name = 'mmc_sbrw_pfrw_h%d_mvavbf_2012' % mass
    leplep.append(higgs_vbf)

    higgs_boo = Histo()
    higgs_boo.input_file  = 'leplep-output/leplep-output-%dll12_boost.root' % mass
    higgs_boo.input_names = ['My_BeforeFitMu1_ggH_', 'My_BeforeFitMu1_VBF_', 'My_BeforeFitMu1_WH_', 'My_BeforeFitMu1_ZH_']
    higgs_boo.output_name = 'mmc_sbrw_pfrw_h%d_mvaboost_2012' % mass
    leplep.append(higgs_boo)



#################################################################
## HADHAD dictionary for plots                                 ##
#################################################################

hadhad = []

## Data
data_vbf = Histo()
data_vbf.input_file  = 'hadhad_workspace/hh_combination_125.root'
data_vbf.input_names = ['channel_vbf_125_mmc1_mass_hists/data/hh_category_vbf_Data_125_mmc1_mass']
data_vbf.output_name = 'hh_category_vbf_Data_125_150_mmc1_mass'
hadhad.append(data_vbf)

data_boo = Histo()
data_boo.input_file  = 'hadhad_workspace/hh_combination_125.root'
data_boo.input_names = ['channel_boosted_125_mmc1_mass_hists/data/hh_category_boosted_Data_125_mmc1_mass']
data_boo.output_name = 'hh_category_boosted_Data_125_150_mmc1_mass'
hadhad.append(data_boo)


## Fake
fake_vbf = Histo()
fake_vbf.input_file  = 'hadhad-output/hadhad-output-125channel_vbf_125_mmc1_mass.root'
fake_vbf.input_names = ['My_FitError_AfterFit_QCD_']
fake_vbf.error_file  = 'hadhad_workspace/hh_combination_125.root'
fake_vbf.error_names = ['channel_vbf_125_mmc1_mass_hists/QCD/hh_category_vbf_QCD_125_mmc1_mass']
fake_vbf.output_name = 'hh_category_vbf_QCD_125_150_mmc1_mass'
hadhad.append(fake_vbf)

fake_boo = Histo()
fake_boo.input_file  = 'hadhad-output/hadhad-output-125channel_boosted_125_mmc1_mass.root'
fake_boo.input_names = ['My_FitError_AfterFit_QCD_']
fake_boo.error_file  = 'hadhad_workspace/hh_combination_125.root'
fake_boo.error_names = ['channel_boosted_125_mmc1_mass_hists/QCD/hh_category_boosted_QCD_125_mmc1_mass']
fake_boo.output_name = 'hh_category_boosted_QCD_125_150_mmc1_mass'
hadhad.append(fake_boo)


## Ztt
ztt_vbf = Histo()
ztt_vbf.input_file  = 'hadhad-output/hadhad-output-125channel_vbf_125_mmc1_mass.root'
ztt_vbf.input_names = ['My_FitError_AfterFit_Ztt_']
ztt_vbf.error_file  = 'hadhad_workspace/hh_combination_125.root'
ztt_vbf.error_names = ['channel_vbf_125_mmc1_mass_hists/Ztautau/hh_category_vbf_Ztautau_125_mmc1_mass']
ztt_vbf.output_name = 'hh_category_vbf_Ztautau_125_150_mmc1_mass'
hadhad.append(ztt_vbf)

ztt_boo = Histo()
ztt_boo.input_file  = 'hadhad-output/hadhad-output-125channel_boosted_125_mmc1_mass.root'
ztt_boo.input_names = ['My_FitError_AfterFit_Ztt_']
ztt_boo.error_file  = 'hadhad_workspace/hh_combination_125.root'
ztt_boo.error_names = ['channel_boosted_125_mmc1_mass_hists/Ztautau/hh_category_boosted_Ztautau_125_mmc1_mass']
ztt_boo.output_name = 'hh_category_boosted_Ztautau_125_150_mmc1_mass'
hadhad.append(ztt_boo)


## others
others_vbf = Histo()
others_vbf.input_file  = 'hadhad-output/hadhad-output-125channel_vbf_125_mmc1_mass.root'
others_vbf.input_names = ['My_FitError_AfterFit_Other_']
others_vbf.error_file  = 'hadhad_workspace/hh_combination_125.root'
others_vbf.error_names = ['channel_vbf_125_mmc1_mass_hists/Others/hh_category_vbf_Others_125_mmc1_mass']
others_vbf.output_name = 'hh_category_vbf_Others_125_150_mmc1_mass'
hadhad.append(others_vbf)

others_boo = Histo()
others_boo.input_file  = 'hadhad-output/hadhad-output-125channel_boosted_125_mmc1_mass.root'
others_boo.input_names = ['My_FitError_AfterFit_Other_']
others_boo.error_file  = 'hadhad_workspace/hh_combination_125.root'
others_boo.error_names = ['channel_boosted_125_mmc1_mass_hists/Others/hh_category_boosted_Others_125_mmc1_mass']
others_boo.output_name = 'hh_category_boosted_Others_125_150_mmc1_mass'
hadhad.append(others_boo)


for mass in [100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150]:
    ## Higgs mass points
    higgs_vbf = Histo()
    higgs_vbf.input_file  = 'hadhad-output/hadhad-output-{0}channel_vbf_{0}_mmc1_mass.root'.format(mass)
    higgs_vbf.input_names = ['My_BeforeFitMu1_ggH_', 'My_BeforeFitMu1_VBF_', 'My_BeforeFitMu1_WH_', 'My_BeforeFitMu1_ZH_']
    higgs_vbf.output_name = 'hh_category_vbf_Signal_%d_mmc1_mass' % mass
    hadhad.append(higgs_vbf)

    higgs_boo = Histo()
    higgs_boo.input_file  = 'hadhad-output/hadhad-output-{0}channel_boosted_{0}_mmc1_mass.root'.format(mass)
    higgs_boo.input_names = ['My_BeforeFitMu1_ggH_', 'My_BeforeFitMu1_VBF_', 'My_BeforeFitMu1_WH_', 'My_BeforeFitMu1_ZH_']
    higgs_boo.output_name = 'hh_category_boosted_Signal_%d_mmc1_mass' % mass
    hadhad.append(higgs_boo)
