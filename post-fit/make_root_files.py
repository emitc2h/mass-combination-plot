import ROOT
from dictionaries import lephad, convert_lephad
from dictionaries import leplep, convert_leplep
from dictionaries import hadhad, convert_hadhad

## lephad output root file
lephad_output = ROOT.TFile('lephad.root', 'RECREATE')
for histo in lephad:
    histo.get(convert_lephad, lephad_output)

lephad_output.Close()


## leplep output root file
leplep_output = ROOT.TFile('leplep.root', 'RECREATE')
for histo in leplep:
    histo.get(convert_leplep, leplep_output)

leplep_output.Close()


## hadhad output root file
hadhad_output = ROOT.TFile('hadhad.root', 'RECREATE')
for histo in hadhad:
    histo.get(convert_hadhad, hadhad_output)

hadhad_output.Close()
