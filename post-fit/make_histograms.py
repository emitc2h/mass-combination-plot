import ROOT, sys, os
import multiprocessing, time, math, shutil, glob

## Import Nils' script
ROOT.gSystem.Load('DumpPostFitHistos_C.so')

## mass points to prepare
masses = [100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150]

## splitting
splitting = False
try:
    splitting = sys.argv[1]
except IndexError:
    pass

## Specify algorithms
processes = []
algos = []

## Splitting
if splitting:
    folds = int(splitting.split(':')[1])
    part  = int(splitting.split(':')[0])

## Origin Workspace
origin_workspace = 'tt12_bv_rCR_125_combined_AllSys_model_0.root'
snapshot         = 'unconditional'
    
## Create the jobs
j=0

## -------------------------------------------------------------
## LEPHAD
## -------------------------------------------------------------
## Clean up and recreate output directory
directory   = 'lephad-output'
try: shutil.rmtree(directory)
except OSError: pass
os.mkdir(directory)

for mass in masses:
    print
    print 'Setting up process for lephad, %d GeV mass point' % mass
    print '---'
    
    ## Make directories for Nils' script output files
    output_file = '%s/lephad-output-%d.root' % (directory, mass)
    workspace   = 'lephad_workspace/Httlh_ajet_embsyks95sths1crand_mmc_mass_lnsobw_H%d_combined_AllSys_model.root' % mass

    if splitting:
        if j % folds == part:
                    
            print '... lephad %d : process %d' % (mass, j)
            p = multiprocessing.Process(target=ROOT.DumpPostFitHistos, args=(origin_workspace, workspace, output_file, snapshot, True, True))
            processes.append(p)

    else:
        print '... lephad %d : process %d' % (mass, j)
        p = multiprocessing.Process(target=ROOT.DumpPostFitHistos, args=(origin_workspace, workspace, output_file, snapshot, True, True))
        processes.append(p)

    j += 1

## -------------------------------------------------------------
## LEPLEP
## -------------------------------------------------------------
## Clean up and recreate output directory
directory   = 'leplep-output'
try: shutil.rmtree(directory)
except OSError: pass
os.mkdir(directory)

for mass in masses:
    print
    print 'Setting up process for leplep, %d GeV mass point' % mass
    print '---'
    
    ## Make directories for Nils' script output files
    output_file = '%s/leplep-output-%d.root' % (directory, mass)
    workspace   = 'leplep_workspace/ws_%s_combined_AllSYS_model.root' % mass

    if splitting:
        if j % folds == part:
                    
            print '... leplep %d : process %d' % (mass, j)
            p = multiprocessing.Process(target=ROOT.DumpPostFitHistos, args=(origin_workspace, workspace, output_file, snapshot, True, True))
            processes.append(p)

    else:
        print '... leplep %d : process %d' % (mass, j)
        p = multiprocessing.Process(target=ROOT.DumpPostFitHistos, args=(origin_workspace, workspace, output_file, snapshot, True, True))
        processes.append(p)

    j += 1


## -------------------------------------------------------------
## HADHAD
## -------------------------------------------------------------
## Clean up and recreate output directory
directory   = 'hadhad-output'
try: shutil.rmtree(directory)
except OSError: pass
os.mkdir(directory)

for mass in masses:
    print
    print 'Setting up process for hadhad, %d GeV mass point' % mass
    print '---'
    
    ## Make directories for Nils' script output files
    output_file = '%s/hadhad-output-%d.root' % (directory, mass)
    workspace   = 'hadhad_workspace/hist2workspace_combined_measurement_hh_combination_%d_model.root' % mass

    if splitting:
        if j % folds == part:
                    
            print '... hadhad %d : process %d' % (mass, j)
            p = multiprocessing.Process(target=ROOT.DumpPostFitHistos, args=(origin_workspace, workspace, output_file, snapshot, True, True))
            processes.append(p)

    else:
        print '... hadhad %d : process %d' % (mass, j)
        p = multiprocessing.Process(target=ROOT.DumpPostFitHistos, args=(origin_workspace, workspace, output_file, snapshot, True, True))
        processes.append(p)

    j += 1



##########################################################################
## Run Nils' scripts (parallelized)                                     ##
##########################################################################
    
running = 0
running_processes = []
limit_running = 30

for i, p in enumerate(processes):
    
    if not running >= limit_running:

        print 'STARTING PROCESS %d' % i
        
        p.start()
        running_processes.append(p)
        running += 1
    else:

        none_finished = True
        finished_processes = []

        while none_finished:
            for j, pro in enumerate(running_processes):
                if not pro.is_alive():
                    pro.join()
                    none_finished = False
                    running -= 1
                    finished_processes.append(j)

            time.sleep(60)

        ## Remove the finished processes
        for pro in reversed(finished_processes):
            running_processes.pop(pro)
        
        print 'STARTING PROCESS %d' % i
        
        p.start()
        running_processes.append(p)
        running += 1

## Make sure all processes are joined
for p in processes:
    p.join()
    
print 'All done.'
